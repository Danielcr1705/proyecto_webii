<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table("users")->insert([
        	"name"=> 'Daniel Calderon',
        	"email"=>'daniel|'."@gmail.com",
        	"password"=>bcrypt("12345678"),
        	'bloquear'=> false,
        	'admi'=> true,
            'created_at' => new DateTime,
            'updated_at' => new DateTime        	
       	]);
    }
}
