<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="{{URL::asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/login_styles.css')}}">
    <script src="{{URL::asset('js/prefixfree.min.js')}}"></script>
  </head>

  <body>

    <div id="login">
    <form method="POST" action="/auth/login">
    {!! csrf_field() !!}
        <div class="field_container">
        <!-- value="{{ old('email') }}" -->
            <input type="email" name="email" value="{{ old('email') }}" placeholder="Email Address">
        </div>
        <div class="field_container">
            <input type="password" name="password" id="password" placeholder="Password">
            <!-- <button type="submit">Login</button> -->
          <button id="sign_in_button" type="submit">
            <span class="button_text">Login</span>
          </button>
        </div>
        <!-- <h2>
          Need to <a href="#" class="login_link">sign up</a> for an account
          or <a href="#" id="forgot_password_link" class="login_link">reset</a> your password?
        </h2> -->
      </div>
    </div>
  </body>
</html>
