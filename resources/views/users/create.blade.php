<?php if (Auth::user()->admi == "true"): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Usuario crear</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="{{URL::asset('default.css')}}">
</head>
<body>
<!-- start header -->
<div id="header">
    <div id="logo">
        <h1><a href="/">Bank</a></h1>
        <h2><a href="/">of Costa Rica</a></h2>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="/home">home</a></li>
            <li><a href="/users">Usuarios</a></li>
            <li><a href="/cuentas">Cuentas</a></li>
            <li><a href="/cajeros">Cajero</a></li>
            <li><a href="/auth/logout">Logout</a></li>
        </ul>
    </div>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
    <!-- start content -->
    <div id="content">
        <div id="latest-post" class="post">
            <h1 class="title">Crear Usuario</h1>
            <form method="POST" action="/users">
            {!! csrf_field() !!}
                <div>
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="name" value="{{ old('name') }}">
                </div>

                <div>
                    <label for="email">Email:</label>
                    <input type="email" name="email" value="{{ old('email') }}">
                </div>

                <div>
                    <label for="password">Password:</label>
                    <input type="password" name="password">
                </div>

                <div>
                    <label for="confirmar">Confirmar Password:</label>
                    <input type="password" name="password_confirmation">
                </div>
                <div>
                    <label for="admi">Aministrador:</label>
                    <select name="admi">
                        <option value="True">Administrador</option>
                        <option value="False">Usuario</option>
                    </select>
                </div>

                <div>
                    <input type="hidden" name="bloquear" value="False">
                </div>

                <div>
                    <button type="submit">Register</button>
                </div>
            </form>
        </div>

    </div>
    <!-- end content -->

    <!-- start sidebar -->
    <div id="sidebar">

    </div>
    <!-- end sidebar -->
</div>
<!-- end page -->
<div id="footer">
    <p id="legal">&copy; All Rights Reserved. | Designed by Isa and Dani</a></p>
</div>
</body>
</html>
@else
    <h1>Acceso Denegado</h1>
    <a href="/home">Atras</a>
<?php endif ?>
