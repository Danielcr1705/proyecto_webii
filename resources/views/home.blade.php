<?php if (Auth::user()->admi == "true"): ?>
<!--
<a href="/users">Usuarios</a>
<a href="/cuentas">Cuentas</a>
<a href="/cajeros">Cajero</a>
<br/>
<a href="auth/logout">logout</a> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Bank of Costa Rica</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('default.css')}}">
</head>
<body>
<!-- start header -->
<div id="header">
    <div id="logo">
        <h1><a href="/home">Bank</a></h1>
        <h2><a href="/home">of Costa Rica</a></h2>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="/home">home</a></li>
            <li><a href="/users">Usuarios</a></li>
            <li><a href="/cuentas">Cuentas</a></li>
            <li><a href="/cajeros">Cajero</a></li>
            <li><a href="/auth/logout">Logout</a></li>
        </ul>
    </div>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
    <!-- start content -->
    <div id="content">
        <!-- start latest-post -->
        <div id="latest-post" class="post">
            <h1 class="title">Welcome to Our Bank!</h1>

            <form method="POST" action="cajeros/retiro">
            {!! csrf_field() !!}
                <input type="text" placeholder="Monto a retirar" name="monto" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                
                <div>
                    <label for="accion">Retiro:</label>
                    <select name="accion">
                        <option value="true">Descendente</option>
                        <option value="false">Ascendente</option>
                    </select>
                </div>

                <h2>Escoja la cuenta donde retirar</h2>
                <table>
                    <tr>
                        <th>Moneda</th>
                        <th>Saldo</th>
                        <th></th>
                    </tr>
                    <?php foreach ( Auth::user()->cuentas as $cuenta): ?>
                    <tr>
                        <td>{{$cuenta->moneda}}</td>
                        <td>{{$cuenta->monto}}</td>
                        <td>    
                            <input type ="submit" name ="retirar" value ="Retirar" data-id="{{$cuenta->id}}"
                                onclick="document.getElementById('selected-id').value = this.dataset.id"/>
                        </td>
                    </tr>
                @endforeach


                </table>   
                <input type="hidden" value="" id="selected-id" name="selected_id">
            </form>
            <a href="/">Atras</a>
        </div>
        <!-- end latest-post -->
    </div>
    <!-- end content -->
</div>
<!-- end page -->
<!-- <div id="footer">
    <p id="legal">&copy; All Rights Reserved. | Designed by Isa and Dani</a></p>
</div> -->
</body>
</html>

@else
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Bank of Costa Rica</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('default.css')}}">
</head>
<body>
<!-- start header -->
<div id="header">
    <div id="logo">
        <h1><a href="/">Bank</a></h1>
        <h2><a href="/">of Costa Rica</a></h2>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="/">home</a></li>
           <!--  <li><a href="/users">Usuarios</a></li>
            <li><a href="/cuentas">Cuentas</a></li>
            <li><a href="/cajeros">Cajero</a></li> -->
            <li><a href="/auth/logout">Logout</a></li>
        </ul>
    </div>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
    <!-- start content -->
    <div id="content">
        <!-- start latest-post -->
        <div id="latest-post" class="post">
            <h1 class="title">Welcome to Our Bank!</h1>
            <form method="POST" action="cajeros/retiro">
            {!! csrf_field() !!}
                <input type="text" placeholder="Monto a retirar" name="monto" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                
                <div>
                    <label for="accion">Retiro:</label>
                    <select name="accion">
                        <option value="true">Descendente</option>
                        <option value="false">Ascendente</option>
                    </select>
                </div>

                <h2>Escoja la cuenta donde retirar</h2>
                <table>
                    <tr>
                        <th>Moneda</th>
                        <th>Saldo</th>
                        <th></th>
                    </tr>
                    <?php foreach ( Auth::user()->cuentas as $cuenta): ?>
                    <tr>
                        <td>{{$cuenta->moneda}}</td>
                        <td>{{$cuenta->monto}}</td>
                        <td>    
                            <input type ="submit" name ="retirar" value ="Retirar" data-id="{{$cuenta->id}}"
                                onclick="document.getElementById('selected-id').value = this.dataset.id"/>
                        </td>
                    </tr>
                @endforeach


                </table>   
                <input type="hidden" value="" id="selected-id" name="selected_id">
            </form>

        </div>
        <!-- end latest-post -->
    </div>
    <!-- end content -->
</div>
<!-- end page -->
<!-- <div id="footer">
    <p id="legal">&copy; All Rights Reserved. | Designed by Isa and Dani</a></p>
</div> -->
</body>
</html>
<?php endif ?>
