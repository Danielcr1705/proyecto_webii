<?php if (Auth::user()->admi == "true"): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Cajeros</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="{{URL::asset('default.css')}}">
</head>
<body>
<!-- start header -->
<div id="header">
    <div id="logo">
        <h1><a href="/home">Bank</a></h1>
        <h2><a href="/home">of Costa Rica</a></h2>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="/home">home</a></li>
            <li><a href="/users">Usuarios</a></li>
            <li><a href="/cuentas">Cuentas</a></li>
            <li><a href="/cajeros">Cajero</a></li>
            <li><a href="/auth/logout">Logout</a></li>
        </ul>
    </div>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
    <!-- start content -->
    <div id="content">
        <div id="latest-post" class="post">
            <h1 class="title">Billetes dispensados:</h1>
            <p>{!! $billetes[0] !!} Billetes de 2000</p>
            <p>{!! $billetes[1] !!} Billetes de 5000</p>
            <p>{!! $billetes[2] !!} Billetes de 10000</p>
            <p>{!! $billetes[3] !!} Billetes de 20000</p>
            <p>{!! $billetes[4] !!} Billetes de 50000</p>
        </div>

    </div>
    <!-- end content -->

    <!-- start sidebar -->
    <div id="sidebar">

    </div>
    <!-- end sidebar -->
</div>
<!-- end page -->
<div id="footer">
    <p id="legal">&copy; All Rights Reserved. | Designed by Isa and Dani</a></p>
</div>
</body>
</html>
@else
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Cajeros</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="{{URL::asset('default.css')}}">
</head>
<body>
<!-- start header -->
<div id="header">
    <div id="logo">
        <h1><a href="/home">Bank</a></h1>
        <h2><a href="/home">of Costa Rica</a></h2>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="/home">home</a></li>
         <!--    <li><a href="/users">Usuarios</a></li>
            <li><a href="/cuentas">Cuentas</a></li>
            <li><a href="/cajeros">Cajero</a></li> -->
            <li><a href="/auth/logout">Logout</a></li>
        </ul>
    </div>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
    <!-- start content -->
    <div id="content">
        <div id="latest-post" class="post">
            <h1 class="title">Billetes dispensados:</h1>
            <p>{!! $billetes[0] !!} Billetes de 2000</p>
            <p>{!! $billetes[1] !!} Billetes de 5000</p>
            <p>{!! $billetes[2] !!} Billetes de 10000</p>
            <p>{!! $billetes[3] !!} Billetes de 20000</p>
            <p>{!! $billetes[4] !!} Billetes de 50000</p>
        </div>

    </div>
    <!-- end content -->

    <!-- start sidebar -->
    <div id="sidebar">

    </div>
    <!-- end sidebar -->
</div>
<!-- end page -->
<div id="footer">
    <p id="legal">&copy; All Rights Reserved. | Designed by Isa and Dani</a></p>
</div>
</body>
</html>


<?php endif ?>
