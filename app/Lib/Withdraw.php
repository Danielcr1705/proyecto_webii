<?php

namespace App\Lib;

class withdraw {

	public static function high_to_low($amount, $denominacion_arr, $cantidad_arr)
	{
		$withdraw = new withdraw;
		$amount_temp = $amount;
		if ($withdraw->validar_monto($amount) && $withdraw->validar_billetes($amount, $cantidad_arr)) {
			for ($i=(count($denominacion_arr)-1); $i >=0 ; $i--) { 
				if ($cantidad_arr[$i]>0) {
					while ($amount_temp >= $denominacion_arr[$i]) {
						if ($cantidad_arr[$i]>0) {
							$amount_temp-=$denominacion_arr[$i];
							$cantidad_arr[$i]-=1;
						}
					}
					if ($amount_temp==1000 || $amount_temp==3000) {
						$amount_temp+=$denominacion_arr[$i];
						$cantidad_arr[$i]+=1;
					}
				}
			}
		}
		return $cantidad_arr;
	}

	public static function low_to_high($amount, $denominacion_arr, $cantidad_arr)
	{
		$amount_temp = $amount;
		$cantidad_arr_backup = $cantidad_arr;

		$withdraw = new withdraw;
		if ($withdraw->validar_monto($amount) && $withdraw->validar_billetes($amount, $cantidad_arr)) {

			for ($i=0; $i < count($denominacion_arr) ; $i++) { 
				//while ($amount_temp>0) {
					//var_export($amount_temp);
				if ($cantidad_arr[$i]!=0) {

					if ($amount_temp%$denominacion_arr[$i]==0) {
						$bills_temp = $amount_temp/$denominacion_arr[$i];
						if ($cantidad_arr[$i]>=1) {
							if ($bills_temp>=$cantidad_arr[$i]) {
								$bills_temp -= $cantidad_arr[$i];
								$amount_temp = $bills_temp * $denominacion_arr[$i];
								$cantidad_arr[$i]=0;
							}else{
								$cantidad_arr[$i]-=$bills_temp;
								$amount_temp=0;
							}
						}
					}else{
						if (($denominacion_arr[$i]==2000)&&($cantidad_arr[$i+1]>=1)) {
							$amount_temp = $amount_temp - 5000;
							$cantidad_arr[$i+1]=$cantidad_arr[$i+1] - 1;
							$i -= 1;
						}else{
							for ($e=($i-1); $e >=0 ; $e--) { 
								if ($cantidad_arr_backup[$e]>0) {
									$cantidad_arr[$e] += 1;
									$bills_temp += 1;
									$amount_temp = $bills_temp * $denominacion_arr[$e];
									$e=-1;
								}elseif(($e==0) && ($cantidad_arr_backup[$e]==0)){
									$cantidad_arr[$i+1] -=1;
									$amount_temp -= $denominacion_arr[$i+1];
								}
							}	
							$i-=1;	
						}
					}
				//}
				}
			}
			//var_export($cantidad_arr);
			return $cantidad_arr;
		}
	}

	public function validar_monto($amount){
		if ($amount!=1000 || $amount!=3000) {
			if ($amount % 1000 == 0) {
				return true;
			}
		}else{
			return false;#"monto invalido"
		}
	}

	public function validar_billetes($amount, $cantidad_arr){
		if ($cantidad_arr[0]==0) {
			if ($cantidad_arr[1]==0) {
				if ($amount%10000==1) {
					return false;
				}
			}else{
				if ($amount%5000==1) {
					return false;
				}
			}
		}else{
			if ($cantidad_arr[1]==0) {
				if ($amount%2000==1) {
					return false;
				}
			}else{
				return true;
			}
		}
	}

	// public function reduccion_billetes(){

	// }

}