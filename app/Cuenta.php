<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table = 'cuenta';
    protected $fillable = ['moneda','monto', 'id_user'];
    public $timestamps = false;
}
