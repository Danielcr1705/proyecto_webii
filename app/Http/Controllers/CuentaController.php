<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Cuenta;
use App\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class CuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuentas = Cuenta::all();
        return view('cuentas.index', compact('cuentas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('cuentas.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'moneda' => 'required',
            'monto' => 'required|integer',
            'id_user' => 'required'
        );

        $this->validate($request,$rules);

        if ( 0 > $request->monto) {
            return redirect()->back();
        }

        $cuenta = Cuenta::create($request->all());
        return redirect('cuentas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuenta = Cuenta::find($id);
        return view('cuentas.edit', compact('cuenta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = array(
            'monto' => 'required'
        );

        $this->validate($request,$rules);

        //Verifica que el monto sea mayor a cero
        if ( 0 > $request->monto) {
            return redirect()->back();
        }
        //Deposito a la cuenta
        if ($request->accion == 'true') {
            $cuenta = Cuenta::find($id);
            $cuenta->monto += $request->monto;
            $cuenta->save();
            return redirect('cuentas');
        }else{

            $cuenta = Cuenta::find($id);

            //Verifica si la cuenta es en dolares o colones
            if ($cuenta->moneda == 'colones') {
                //Retiro normal de colones
                if ($cuenta->monto >= $request->monto) {
                    $cuenta->monto -= $request->monto;
                    $cuenta->save();
                    return redirect('cuentas');
                }

                return redirect()->back();

            }else{
                //Convertidor de colones a dolares.
                $cambio = floor($request->monto / 526.44); //Hace el cambio a dolares y lo redondea

                //Retira la cantidad de dolares que resultó de la conversión.
                if ($cuenta->monto >= $cambio) {
                    $cuenta->monto -= $cambio;
                    $cuenta->save();
                    return redirect('cuentas');
                }
                return redirect()->back();
            }
        /*Precio dolar: 526,44*/
        }
    }
    public function cajero_retiro_update($amount, $id){
        $cuenta = Cuenta::find($id);
        if ($cuenta->moneda == 'colones') {
            $cuenta->monto -= $amount;
            $cuenta->save();
        }else{
            $cambio = floor($amount / 526.41);
            $cuenta->monto -= $cambio;
            $cuenta->save();
        }
    }

    public static function current_user_accounts(){
        //Auth::user()->id;
        //$sql = 'SELECT * FROM cuenta WHERE id_user = ' . Auth::user()->id ;      DB::select($sql);
        //"SELECT * FROM .' cuenta '. WHERE .' id_user ' . = Auth::user()->id "
        $result = Auth::user()->cuentas;
        return view('cuentas.show', compact('result'));
    }
}
