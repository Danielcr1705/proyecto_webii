<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cajero;
use App\Lib\Withdraw;
use App\Cuenta;
use DB;
use View;
use App\Http\Controllers\CuentaController;

class CajeroController extends Controller
{
    public $denominacion_arr = array();
    public $cantidad_arr = array();
    public $diff_billetes_usados =  array();
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cajeros = Cajero::orderBy('denominacion', 'ASC')->get();
        return view('cajeros.index', compact('cajeros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cajeros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request->all());
        $cajero = new Cajero();
        $cajero->denominacion = $request->denominacion;
        $cajero->cantidad = $request->cantidad;
        $cajero->save();
        return redirect('cajeros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $cajero = Cajero::find($id);
         return view('cajeros.show', compact('cajero'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cajero = Cajero::find($id);
        return view('cajeros.edit', compact('cajero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $cajero = Cajero::find($id);
        $cajero->denominacion = $request->denominacion;
        $cajero->cantidad += $request->cantidad;
        $cajero->save();
        return redirect('cajeros');
    }

    public function retiro(Request $request){
        $amount = $request->monto;
        $id = $request->selected_id;
        //Request::input('name');
        // var_export($id);
        // var_export($amount);


        $cuenta = Cuenta::find($id);
        $monto_cuenta = $cuenta->monto;
        if ($cuenta->moneda == 'dolares') {
           //$amount = floor($amount * 526.44);
           $monto_cuenta *= 526.44;
           //var_export($amount)
        }
        if (($amount <= $this->total_cajero()) && $monto_cuenta >= $amount ) {
            if ($request->accion == 'true') {
                $cajero = Cajero::orderBy('denominacion', 'DESC')->get();
                $this->cajero_to_array($cajero);
                $billetes = Withdraw::high_to_low($amount, $this->denominacion_arr, $this->cantidad_arr);
                if ($billetes==null) {
                    return redirect('/home');
                }else{
                    //var_export($billetes);
                    $this->denominacion_arr = array();
                    $this->cantidad_arr = array();
                    $cajero = Cajero::orderBy('denominacion', 'ASC')->get();
                    $this->cajero_to_array($cajero);
                    $this->diff_billetes_usados = $this->billetes_usados($billetes);
                    $this->reduccion_billetes($billetes);
                    $cuenta = new CuentaController;
                    $cuenta->cajero_retiro_update($amount,$id);
                    return view('cajeros.show');
                }

            }else{
                $cajero = Cajero::orderBy('denominacion', 'ASC')->get();
                $this->cajero_to_array($cajero);
                $billetes = Withdraw::low_to_high($amount, $this->denominacion_arr, $this->cantidad_arr);
                //var_export($billetes);
                if ($billetes==null) {
                    return redirect('/home');
                }else{
                    //var_export($billetes);
                    $this->diff_billetes_usados = $this->billetes_usados($billetes);
                    $this->reduccion_billetes($billetes);
                    $cuenta = new CuentaController;
                    $cuenta->cajero_retiro_update($amount,$id);
                    return view('cajeros.show');
                }
            }
        }else
        {
            return redirect('/home');
        }
    }

    public function total_cajero(){
        $cajeros = Cajero::all();
        $total=0;
        foreach ($cajeros as $cajero) {
            $total += ((int)$cajero->denominacion * (int)$cajero->cantidad);
        }
        return $total;
    }

    public function cajero_to_array($cajeros){
        $cajeros = Cajero::orderBy('denominacion', 'ASC')->get();
        foreach($cajeros as $cajero) {
            $this->denominacion_arr[] = (int) $cajero->denominacion;
            $this->cantidad_arr[]= (int) $cajero->cantidad;
        }
    }

    public function total_cuenta($id){
        $cuenta = Cuenta::find($id);
        if ($cuenta->monto >= $request->monto) {
            $cuenta->monto -= $request->monto;
            $cuenta->save();
            return redirect('cuentas');
        }
    }

    public function reduccion_billetes($billetes){
        //var_export($billetes);
        for ($i=0; $i < (count($this->denominacion_arr)) ; $i++) {
            $cajero = Cajero::find($i+1);
            $cajero->cantidad = $billetes[$i];
            $cajero->save();
        }
    }

    public function billetes_usados($billetes){
        $temp_arr = array();
        for ($i=0; $i < count($this->denominacion_arr) ; $i++) {
            $temp_arr[$i] = $this->cantidad_arr[$i] - $billetes[$i];
        }
        View::share('billetes',$temp_arr);
        return $temp_arr;
    }
}
