<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('home', function () {
    return view('home');
});
Route::get('/', function () {
    return view('auth/login');
});
Route::post('cajeros/retiro', 'CajeroController@retiro');
Route::resource('cajeros', 'CajeroController');
Route::resource('users', 'UserController');
Route::resource('cuentas', 'CuentaController');

Route::get('foo', ['middleware' => 'GrahamCampbell\Throttle\Http\Middleware\ThrottleMiddleware:3,30', function () {
    return 'Success';
}]);
